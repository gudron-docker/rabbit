#!/bin/bash
set -e

rabbitmq_server_cmd=( /usr/lib/rabbitmq/bin/rabbitmq-server )
rabbitmqctl_cmd=( rabbitmqctl )

if [ "$(id -u)" = '0' ]; then

    echo 'Initializing rabbitmq'

    mkdir -p /var/run/rabbitmq/
    chown -R rabbitmq:rabbitmq /var/lib/rabbitmq /etc/rabbitmq /var/run/rabbitmq/
    chmod 777 /var/lib/rabbitmq /etc/rabbitmq
    ln -nsf /var/lib/rabbitmq/.erlang.cookie /root/

    exec gosu rabbitmq "$BASH_SOURCE"
fi

echo "Initializing run rabbitmq"

/usr/lib/rabbitmq/bin/rabbitmq-server &
rabbitmqctl wait "$RABBITMQ_PID_FILE"

for file in /opt/docker-entrypoint-init.d/*; do
    if [ -f ${file} ];
    then
        case "$file" in
            *.sh)     echo "$0: running $file"; . "$file" ;;
            *)        echo "$0: ignoring $file" ;;
        esac
    fi
    echo
done

rabbitmqctl stop "$RABBITMQ_PID_FILE"
sleep 2

exec /usr/lib/rabbitmq/bin/rabbitmq-server